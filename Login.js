/**
 * The export in this file has many exit points. They happen when `returnData` is set.
 * `if (err) { returnData = { ok: false, error: 0 }; }` < This piece of code sets
 * the value of `returnData`, and any subsequent code will attempt to move the current
 * position back to the lambda expression so the value can be returned.
 */

"use strict";

const fs = require("fs");
let mysql = require("mysql");
let bcrypt = require("bcrypt");
let crypto = require("crypto");

// Giving lambda expression as return value, called as `require("")()`
module.exports = (uname, pword, uuid, callback, resobject) => {
  // Getting information from a config file (will crash when there is none)
  let connJson = JSON.parse(fs.readFileSync("/etc/teamtimeapi/config.json"));
  let connection = mysql.createConnection({
    host: connJson.host,
    user: connJson.username,
    password: connJson.password,
    database: connJson.database,
    charset: "utf8mb4"
  });

  // Letting variables be for later use
  let returnData;
  let successData;

  // Establish connection to database
  connection.connect(err => {
    if (err) { callback(false, 0, resobject); return; }

    // Getting the requested user and it's information from the database
    connection.query("SELECT userPassword, userDisplayName, userEmail, userPhoneUUID, userIsAdmin FROM Users WHERE userLoginName = ?", [uname],
    (err, res, fields) => {
      if (err) { callback(false, 0, resobject); return; }

      // Letting variables be for itteration purposes
      let isOk = false;
      let row;
      // Looping through results, setting `row` when bcrypt finds a match
      for (var i = 0; i < res.length; ++i) {
        row = res[i];
        if (bcrypt.compareSync(pword, row.userPassword)) { isOk = true; break; }
      }
      if (!isOk) { callback(false, 1, resobject); return; }

      // Checking if UUID does not match
      if (uuid !== row.userPhoneUUID && row.userPhoneUUID !== "") {
        // Deleting current Token
        connection.query("DELETE FROM Tokens WHERE tokenUser = ?", [uname], err => {
          if (err) { callback(false, 0, resobject); }
          else {
            connection.query("UPDATE Users SET userPhoneUUID='' WHERE userLoginName = ?", [uname], err => {
              if (err) { callback(false, 0, resobject); }
              else { callback(false, 2, resobject); }
            });
          }
        });
        return;
      }



      // Changing the UUID for the user's phone
      connection.query("UPDATE Users SET userPhoneUUID=? WHERE userLoginName=?", [uuid, uname], err => {
        if (err) { callback(false, 0, resobject); return; }
        else {
          // Making a new token
          let token = crypto.randomBytes(32).toString("hex");
          connection.query("INSERT INTO Tokens VALUES (?, ?, DATE_ADD(NOW(), INTERVAL 24 HOUR)) ON DUPLICATE KEY UPDATE tokenContent=?",
          [uname, token, token], err => {
            if (err) { callback(false, 0, resobject); return; }
            else { callback(true, { ok: true, token: token, name: row.userDisplayName, email: row.userEmail, admin: (row.userIsAdmin === 1) ? true : false }, resobject); }
          });
        }
      });

    });

  });

};
