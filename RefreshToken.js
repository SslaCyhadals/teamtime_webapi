"use strict";

const fs = require("fs");
let mysql = require("mysql");
let crypto = require("crypto");

// Giving lambda expression as return value, called as `require("")()`
module.exports = (uname, uuid) => {
  // Getting information from a config file (will crash when there is none)
  let connJson = JSON.parse(fs.readFileSync("/etc/teamtimeapi/config.json"));
  let connection = mysql.createConnection({
    host: connJson.host,
    user: connJson.username,
    password: connJson.password,
    database: connJson.database
  });

  let returnData;

  // Establish connection to database
  connection.connect(err => {
    if (err) { returnData = { ok: false, error: 0 }; }
  });
  if (returnData) { return returnData; }

  connection.query("SELECT userLoginName, userPhoneUUID, tokenContent FROM Users RIGHT JOIN Tokens ON userLoginName = tokenUser WHERE tokenValidUntil <= NOW() AND userLoginName = ?",
  [uname], (err, res, fields) => {
    if (err) { returnData = { ok: false, error: 0 }; return; }

    // Checking length of results
    if (res.length === 0) { returnData = { ok: false, error: 1 }; return; }

    // Comparing UUIDs
    if (uuid !== res[0].userPhoneUUID) { returnData = { ok: false, error: 2 }; return; }

    // Renewing Token validity timestamp
    connection.query("UPDATE ")
  });

  return returnData;
};
