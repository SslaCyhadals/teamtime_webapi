"use strict";

const fs = require("fs");
const express = require("express");
var https = require("https");
var bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.json());

let connJson = JSON.parse(fs.readFileSync("/etc/teamtimeapi/config.json"));



// Catching POST requests on /auth
app.post("/auth", (req, res) => {
  if (connJson.verbose) { console.log(`POST received on /auth`); }

  if (req.body.username === undefined || req.body.password === undefined || req.body.uuid === undefined) {
    if (connJson.verbose) { console.log("Invalid parameters, closing connection"); }
    res.statusMessage = "Invalid parameters";
    res.status(400).end(); // Invalid Parameters
    return;
  }

  // Attempting login with parameters
  require("./Login.js")(req.body.username, req.body.password, req.body.uuid, authCallback, res);
});
app.get("/auth", (req, res) => {
  if (connJson.verbose) { console.log("No m8, use POST"); }
  res.statusMessage = "Upgrade to Windows 11 NOW!";
  res.status(426).end(); // Invalid Parameters
});

app.post("/rftoken", (req, res) => {
  if (connJson.verbose) { console.log("CHUCK NORRIS"); }
  res.statusMessage = "Reserved for Chuck Norris";
  res.status(749);
  res.send("<h1>Reserved for Chuck Norris</h1><p>He will be here soon, trust me</p>");
  // TODO: Add "functionality"
});


app.get("/teapot", (req, res) => {
  if (connJson.verbose) { console.log("NOT A TEAPOT"); }
  res.statusMessage = "I am not a teapot";
  res.status(718);
  res.send("<h1>I am not a teapot</h1><p>Eff off with your weird pottery</p>");
});



// Listening to requests from port 3000
let port = 3000;
https.createServer({
  key: fs.readFileSync("/etc/letsencrypt/live/api.teamtime.eu/privkey.pem"),
  cert: fs.readFileSync("/etc/letsencrypt/live/api.teamtime.eu/cert.pem")
}, app).listen(port, () => {
  console.log(`Listening on port ${port}...`);
});



/**=========================================**
 * Callback functions for after Async things *
 **=========================================**/

function authCallback(isok, out, res) {
/*   if (result === undefined) { if (connJson.verbose) { console.log("Fucking Race Conditions"); }
                                  res.statusMessage = "Fucking Race Conditions"; res.status(736).end(); return; } */
  if (isok === false) {
    if (connJson.verbose) { console.log("Login failed, closing connection"); }
    if (out === 1) { res.statusMessage = "Authentication failed"; res.status(401).end(); } // Authentication failed
    else if (out === 2) { res.statusMessage = "UUID mismatch"; res.status(409).end(); } // Token mismatch on user
    else if (out === 0) { res.statusMessage = "Internal Server Error"; res.status(500).end(); } // Query error
    return;
  }

  // Sending a response back to the client
  if (connJson.verbose) { console.log("Login OK, sending data"); }
  res.type("json");
  res.send({ token: out.token, name: out.name, email: out.email, admin: out.admin });
}
